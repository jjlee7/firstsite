"""firstsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
#import admin,path,url....函式庫
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include


#import view functions from your app here.
from firstsite.views import here, welcome
from classduty.views import welcome_classduty, dayoff, reduty, punishment, classduty


# urlpatterns = [
#    url(r'^admin/', admin.site.urls),
#    url(r'^hello/', hello_view),
# ]

#定義路徑(url)清單從這邊開始
urlpatterns = [
      path('admin/', admin.site.urls),
    #   path('here/', here),
    #   path('<int:a>/math/<int:b>',math),
    #   path('food/',food),
    #   path('menu/<int:id>/',menu),
    #   path('welcome/',welcome),
    #   path('restaurants_list/',restaurants_list),
    #   path('comments/<int:id>/',comment),
      path('welcome_classduty/',welcome_classduty),
      path('dayoff/',dayoff),
      path('reduty/',reduty),
      path('punishment/',punishment),
      path('classduty/',classduty),

]