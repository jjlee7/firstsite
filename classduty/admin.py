from django.contrib import admin

# Register your models here.

from classduty.models import Studentindex, Reduty, Punishment, Classduty

class StudentindexAdmin(admin.ModelAdmin):
    list_display = ('code','name','grade')
    search_fields = ('code',)

class RedutyAdmin(admin.ModelAdmin):
    list_display = ('update_time','code','name')
    list_filter = ('update_time',) # need to add ',' in the end if only one element in tuple


class PunishmentAdmin(admin.ModelAdmin):
    list_display = ('update_time','code','name')
    list_filter = ('update_time',)

class ClassdutyAdmin(admin.ModelAdmin):
    list_display = ('update_time','board','recycle','ricebox','vegebox1','vegebox2','soupbox','platebox1','platebox2','prenum')
    list_filter = ('update_time',)


admin.site.register(Studentindex,StudentindexAdmin)
admin.site.register(Reduty,RedutyAdmin)
admin.site.register(Punishment,PunishmentAdmin)
admin.site.register(Classduty,ClassdutyAdmin)