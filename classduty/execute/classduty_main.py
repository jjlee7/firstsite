
# coding: utf-8

# In[ ]:


from classduty.models import Studentindex, Dayoff, Reduty, Punishment, Classduty

from datetime import datetime,date

from classduty.execute.classduty_fuction import getYesterday,dayoff_today,classduty_yesterday,prenum_yesterday,punishment_unfinished,reduty_unfinished,punishment_finished_update,reduty_finished_update,classduty_today_restore


#########################################主程式#######################################################

def classduty_main():
    import random
    
    students = len(Studentindex.objects.all())
    dutyitems = len(['board','recycle','ricebox','vegebox1','vegebox2','soupbox','platebox1','platebox2'])

    classduty_pre = []
    for i in range(prenum_yesterday(),prenum_yesterday()+dutyitems):
        if i>students:
            i = i - students
        classduty_pre.append(i)

    if set(classduty_pre) & set(dayoff_today()) == {}:
        classduty_box=classduty_pre
        pass
    else:
        startnum = max(classduty_pre)+1
        endnum = startnum + len(set(classduty_pre) & set(dayoff_today()))
        classduty_prenum = list(set(classduty_pre) - set(dayoff_today()))
        for i in range(startnum,endnum):
            classduty_prenum.append(i)

    reduty_box = list( ( set(reduty_unfinished())|set(punishment_unfinished()) ) - set(dayoff_today()) ) 
    # (處罰名單|重做名單)-今日請假 = 不重複的redutybox名單

    if len(set(reduty_box) & set(classduty_prenum)) > 2: # (不重複的redutybox名單＆今日預備值日生名單)>2時,必須拿掉部分名單,否則會有bug
        reduty_mul = list( set(reduty_box) & set(classduty_prenum) )
        while len(reduty_mul) > 2:
            num = random.choice(reduty_mul)
            reduty_box.remove(num)
            reduty_mul.remove(num)

    if len(reduty_box) > dutyitems:
        while len(reduty_box) > dutyitems:
            reduty_box.pop()

    for i in range(len(reduty_box)):
        classduty_prenum.pop()

    classduty_box = classduty_prenum

    if len(classduty_box) == 0: #若reduty_box需要重做的人數為8，則classduty_box就會呈現空集合，則prenum沿用昨日
        prenum = prenum_yesterday()
    else:
        prenum = classduty_box[-1]+1

    #prenum_restore(p=prenum) #回存prenum

    classduty_box = classduty_box + reduty_box

    random.shuffle(classduty_box)    

    multitwo = [] #找到出現2次的今日值日生名單
    for i in classduty_box:
        if classduty_box.count(i) == 2:
            multitwo.append(i)
    multitwo.sort()

    for i in multitwo:
        classduty_box.remove(i)

    for i,j in zip(multitwo,[0,dutyitems,0,dutyitems]):
        classduty_box.insert(j,i)       


    for i in reduty_box:#修改punishment_finished_update與reduty_finished_update
        punishment_finished_update = punishment_finished_update(p=i)
        if punishment_finished_update == 1:
            reduty_box.remove(i)
        reduty_finished_update = reduty_finished_update(r=i)
        if reduty_finished_update == 1:
            reduty_box.remove(i)

    classduty_today_restore(c=classduty_box) #回存classduty_today

################################################################################################

