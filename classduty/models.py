from django.db import models

# Create your models here.
import datetime

class Studentindex(models.Model):
    code = models.DecimalField(max_digits=2,decimal_places=0)
    name = models.CharField(max_length=20)
    grade = models.CharField(max_length=20)

    def __str__(self): 
        return self.name

class Dayoff(models.Model):
    update_time = models.DateField(auto_now=False)
    code = models.DecimalField(max_digits=2,decimal_places=0)
    name = models.CharField(max_length=20)

    def __str__(self): 
        return self.name

class Reduty(models.Model):
    update_time = models.DateField(auto_now=False)
    code = models.DecimalField(max_digits=2,decimal_places=0)
    name = models.CharField(max_length=20)
    is_done = models.BooleanField(default=False)

    def __str__(self): 
        return self.name

class Punishment(models.Model):
    update_time = models.DateField(auto_now=False)
    code = models.DecimalField(max_digits=2,decimal_places=0)
    name = models.CharField(max_length=20)
    is_done = models.BooleanField(default=False)
    
    def __str__(self):
        return self.name

class Classduty(models.Model):
    update_time = models.DateField(auto_now=False)
    board = models.DecimalField(max_digits=2,decimal_places=0)
    recycle = models.DecimalField(max_digits=2,decimal_places=0)
    ricebox = models.DecimalField(max_digits=2,decimal_places=0)
    vegebox1 = models.DecimalField(max_digits=2,decimal_places=0)
    vegebox2 = models.DecimalField(max_digits=2,decimal_places=0)
    soupbox = models.DecimalField(max_digits=2,decimal_places=0)
    platebox1 = models.DecimalField(max_digits=2,decimal_places=0,null=True)
    platebox2 = models.DecimalField(max_digits=2,decimal_places=0,null=True)
    prenum = models.DecimalField(max_digits=2,decimal_places=0,null=True)

