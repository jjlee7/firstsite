
# coding: utf-8

# In[ ]:


from classduty.models import Studentindex, Dayoff, Reduty, Punishment, Classduty

########“得到昨天日期”函式########
from datetime import datetime,date

def getYesterday():
    import datetime
    today=datetime.date.today() 
    oneday=datetime.timedelta(days=1) 
    yesterday=today-oneday  
    return yesterday 
################################



########取出今日請假名單list########
def dayoff_today():
    dayoff_today = []
    d_num = list(Dayoff.objects.filter(update_time=date.today()))
    for i in d_num:
        dayoff_today.append(int(i.code))
    return dayoff_today
################################



########取出昨日值日生座號list########
def classduty_yesterday():
    c_query = Classduty.objects.get(update_time=getYesterday())
    board = int(c_query.board)
    recycle = int(c_query.recycle)
    ricebox = int(c_query.ricebox)
    vegebox1 = int(c_query.vegebox1)
    vegebox2 = int(c_query.vegebox2)
    soupbox= int(c_query.soupbox)
    platebox1 = int(c_query.platebox1)
    platebox2 = int(c_query.platebox2)
    classduty_yesterday = [board,recycle,ricebox,vegebox1,vegebox2,soupbox,platebox1,platebox2]
    return classduty_yesterday 
################################



########取出昨日prenum########
def prenum_yesterday():
    c_query = Classduty.objects.get(update_time=getYesterday())
    return int(c_query.prenum)
################################



########取出尚未執行"勞動服務"座號list#########(已按照日期排序)
def punishment_unfinished():
    p_num = list(Punishment.objects.filter(is_done=0).order_by('update_time'))
    punishment_unfinished = []
    for i in p_num:
        punishment_unfinished.append(int(i.code))
    return punishment_unfinished
################################



#########取出尚未執行"重做名單"座號list#########(已按照日期排序)
def reduty_unfinished():
    r_num = list(Reduty.objects.filter(is_done=0).order_by('update_time'))
    reduty_unfinished = []
    for i in r_num:
        reduty_unfinished.append(int(i.code))
    return reduty_unfinished
################################



#########修改已執行"勞動服務"座號#########
def punishment_finished_update(p):
    finished_update = 0
    
    p_sort = Punishment.objects.filter(is_done=0,code=p).order_by('update_time')
    
    if len(p_sort) == 0:
        pass
    else:
        l=[]
        for i in p_sort:
            l.append(str(i.update_time))        
        Punishment.objects.filter(update_time=l[0],code=p).update(is_done=True)
        finished_update = 1
    return finished_update
################################      



#########修改已執行"重做"座號list#########
def reduty_finished_update(r):
    finished_update = 0
    
    r_sort = Reduty.objects.filter(is_done=0,code=r).order_by('update_time')
    
    if len(r_sort) == 0:
        pass
    else:
        l=[]
        for i in r_sort:
            l.append(str(i.update_time))        
        Punishment.objects.filter(update_time=l[0],code=r).update(is_done=True)
        finished_update = 1
    return finished_update
################################



#########回存今日值日生名單list#########
def classduty_today_restore(c):
    board = c[0]
    recycle = c[1]
    ricebox = c[2]
    vegebox1 = c[3]
    vegebox2 = c[4]
    soupbox = c[5]
    platebox1 = c[6]
    platebox2 = c[7]
    c = Classduty(update_time=datetime.date.today(),board=board,recycle=recycle,ricebox=ricebox,vegebox1=vegebox1,vegebox2=vegebox2,soupbox=soupbox,platebox1=platebox1,platebox2=platebox2)
    c.save()
################################



#########回存prenum#########
# def prenum_restore(p):
#     c = Classduty(update_time=date.today(),prenum=p)
#     c.save()
################################



