
from django.shortcuts import render
# Create your views here.
from django.http import HttpResponse

def here(request):
    return HttpResponse("Mom, I'm here!")

def add(request,a,b):
    s=int(a)+int(b)
    return HttpResponse( str(s) )

def welcome(request):
   if 'user_name' in request.GET:
       return HttpResponse('Welcome!~ '+request.GET['user_name'])
   else:
       return render(request,'welcome.html',locals())