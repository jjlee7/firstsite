from django.shortcuts import render,render_to_response
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
from classduty.models import Studentindex, Dayoff, Reduty, Punishment, Classduty
from django.utils import timezone
from datetime import datetime,date
from classduty.execute.classduty_main import classduty_main

# def hello_view(request):
#     return render(request, 'classduty.html', {
#         'data': 'hello',
#     })

# def math(request,a,b):
#     s = a + b
#     d = a - b
#     p = a * b
#     q = a / b
#     return render_to_response('classduty.html',{'s':s,'d':d,'p':p,'q':q})

# def food(request):
#     food1 = {'meal':'steak','price':'29$','spicy':'a little'}
#     item1 = food1.items()
#     food2 = {'meal':'salad','price':'19$','spicy':'less'}
#     item2 = food2.items()
#     items = [item1,item2]


def welcome_classduty(request):
    return render(request,'welcome_classduty.html',{'now':str(date.today())})


def dayoff(request):
     students = Studentindex.objects.all() #似乎不需要這一行
     if 'absence' in request.POST:
         absence_amount = int(request.POST['absence'])
         absence_count = len(request.POST.getlist('code'))
         absence_list = request.POST.getlist('code')
         if absence_amount==absence_count:
             if absence_amount==0:
                 pass
             else:
                 for count in absence_list:
                     update_time = timezone.now()
                     code = int(count)
                 
                     s = Studentindex.objects.get(code=count)
                     name = s.name

                     d = Dayoff(update_time=update_time, code=code, name=name)
                     d.save()
             return HttpResponseRedirect("/welcome_classduty/")          
         else:
             return render(request,'error_dayoff.html',locals())
     else:
         return render(request,'dayoff.html',locals())


def reduty(request):
    students = Studentindex.objects.all() #似乎不需要這一行
    if 'reduty' in request.POST:
        reduty_amount = int(request.POST['reduty'])
        reduty_count = len(request.POST.getlist('code'))
        reduty_list = request.POST.getlist('code')
        if reduty_amount==reduty_count:
            if reduty_amount==0:
                pass
            else:
                for count in reduty_list:
                    update_time = request.POST['date']
                    code = int(count)
                 
                    s = Studentindex.objects.get(code=count)
                    name = s.name

                    r = Reduty(update_time=update_time, code=code, name=name)
                    r.save()
            return HttpResponseRedirect("/welcome_classduty/")
        else:
            return render(request,'error_reduty.html',locals())
    else:
        return render(request,'reduty.html',locals())

def punishment(request):
    students = Studentindex.objects.all() #似乎不需要這一行
    if 'punishment' in request.POST:
        punishment_amount = int(request.POST['punishment'])
        punishment_count = len(request.POST.getlist('code'))
        punishment_list = request.POST.getlist('code')
        if punishment_amount==punishment_count:
            if punishment_amount==0:
                pass
            else:
                for count in punishment_list:
                    update_time = request.POST['date']
                    code = int(count)
                 
                    s = Studentindex.objects.get(code=count)
                    name = s.name

                    p = Punishment(update_time=update_time, code=code, name=name)
                    p.save()
            return HttpResponseRedirect("/welcome_classduty/")
        else:
            return render(request,'error_punishment.html',locals())
    else:
        return render(request,'punishment.html',locals())

def classduty(request):
    classduty_main()
    classduty_arrangement = Classduty.objects.get(update_time=date.today())
 
    return render(request,'classduty.html',locals())